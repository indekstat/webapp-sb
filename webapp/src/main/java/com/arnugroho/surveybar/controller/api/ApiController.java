package com.arnugroho.surveybar.controller.api;

import com.arnugroho.surveybar.model.RSBranch;
import com.arnugroho.surveybar.model.User;
import com.arnugroho.surveybar.model.UserBranch;
import com.arnugroho.surveybar.model.response.DefaultResponse;
import com.arnugroho.surveybar.model.ScheduleDay;
import com.arnugroho.surveybar.model.response.RSBranchResponse;
import com.arnugroho.surveybar.model.response.UserResponse;
import com.arnugroho.surveybar.repository.RSBranchRepository;
import com.arnugroho.surveybar.repository.UserBranchRepository;
import com.arnugroho.surveybar.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserBranchRepository userBranchRepository;
    @Autowired
    private RSBranchRepository rsBranchRepository;

    @GetMapping("/schedules")
    DefaultResponse schedule() {
        List<ScheduleDay> scheduleDays = new ArrayList<>();

        DefaultResponse<List<ScheduleDay>>  response = new DefaultResponse(Boolean.TRUE);
        response.setData(scheduleDays);
        return response;
    }

    @GetMapping("/user/{email}")
    DefaultResponse user(@PathVariable("email") String email){
        UserResponse userResponse = new UserResponse();
        User user = userRepository.findByEmail(email);

        DefaultResponse<User>  response = new DefaultResponse(Boolean.TRUE);

        response.setData(user);
        return response;
    }

    @GetMapping("/userbranch/{email}")
    DefaultResponse userBranch(@PathVariable("email") String email){
        Collection<UserBranch> userBranches = userBranchRepository.findByUserEmail(email);


        DefaultResponse<Collection<UserBranch>>  response = new DefaultResponse(Boolean.TRUE);

        response.setData(userBranches);
        return response;
    }

    @GetMapping("/branchs")
    DefaultResponse branchs(){
        Collection<RSBranch> rsBranches = rsBranchRepository.findAll();
        //Collection<RSBranchResponse> rsBranchResponses = rsBranchRepository.findAllResponse();
        Collection<RSBranchResponse> rsBranchResponses = new ArrayList<>();
        for (RSBranch data : rsBranches){
            RSBranchResponse rsBranchResponse = new RSBranchResponse();
            BeanUtils.copyProperties(data, rsBranchResponse);
            rsBranchResponses.add(rsBranchResponse);
        }

        DefaultResponse<Collection<RSBranchResponse>>  response = new DefaultResponse(Boolean.TRUE);

        response.setData(rsBranchResponses);
        return response;
    }


}
