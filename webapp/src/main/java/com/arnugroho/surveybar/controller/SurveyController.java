package com.arnugroho.surveybar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.arnugroho.surveybar.model.DefaultKeyValue;
import com.arnugroho.surveybar.util.CommonFunction;

@Controller
public class SurveyController {

	@GetMapping("/survey")
	public String survey(@RequestParam(name = "dest", required = true, defaultValue = "home") String dest,
			Model model) {
		DefaultKeyValue defaultKeyValue = CommonFunction.getKey();
		model.addAttribute("kv", defaultKeyValue);
		return dest;
	}

	@PostMapping("/warunga")
	public String saveWarungA(@RequestParam(name = "key", required = true) String key,
			@RequestParam(name = "value", required = true) String value, Model model,
			@ModelAttribute DefaultKeyValue defaultKeyValue) {
		// DefaultKeyValue defaultKeyValue = CommonFunction.getKey();
		model.addAttribute("kv", defaultKeyValue);
		return "greeting";
	}

}
