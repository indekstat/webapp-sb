package com.arnugroho.surveybar.util;

import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.arnugroho.surveybar.model.DefaultKeyValue;


public class CommonFunction {

	public static DefaultKeyValue getHasingCode() {
		DefaultKeyValue defaultKeyValue = new DefaultKeyValue();
		SimpleDateFormat format = new SimpleDateFormat(CommonVariable.DATETIME);
		String date = format.format( new Date());

		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);

		KeySpec spec = new PBEKeySpec(date.toCharArray(), salt, 65536, 128);
		SecretKeyFactory factory;
		try {
			factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

			byte[] hash = factory.generateSecret(spec).getEncoded();
			
			System.out.println(salt + " salt");
			System.out.println(hash + " hash");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return defaultKeyValue;

	}
	
	public static DefaultKeyValue getKey() {
		DefaultKeyValue defaultKeyValue = new DefaultKeyValue();
		//Integer random = (int )(Math.random() * 50 + 1);
		Date date = new Date();
		
		defaultKeyValue.setKey(String.valueOf(date.getTime()));
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		Integer year = calendar.get(Calendar.YEAR) * 2 ;
		Integer month = calendar.get(Calendar.MONTH) * 2 ;
		Integer day = calendar.get(Calendar.DAY_OF_MONTH) * 2 ;
		Long time = calendar.getTimeInMillis();
		
		Long sum = ((year + month + day) + time)*2;
		
		
		defaultKeyValue.setValue(sum.toString());
		
		return defaultKeyValue;
		
	}
	
	public static Boolean verifyKey(Long key, Long value) {
		//Integer random = key;
		Date date = new Date(key);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Integer year = calendar.get(Calendar.YEAR) * 2 ;
		Integer month = calendar.get(Calendar.MONTH) * 2 ;
		Integer day = calendar.get(Calendar.DAY_OF_MONTH) * 2 ;
		Long time = calendar.getTimeInMillis();
		
		Long sum = ((year + month + day) + time)*2;
		
		if(sum.longValue() == value.longValue()) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
		
	}

}
