package com.arnugroho.surveybar.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name= ScheduleAppointment.TABLE_NAME)
@Data
public class ScheduleAppointment extends CommonModel {
    public static final String TABLE_NAME = "t_schedule_appointment";

    @Id
    @Column(name = "id")
    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateAppointment", nullable = false)
    private Date dateAppointment;

    @ManyToOne(fetch = FetchType.LAZY)
    //name merupakan nama column milik scheduleappointment
    @JoinColumn(name = "schedule_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private ScheduleDay scheduleDay;
}
