package com.arnugroho.surveybar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

//import com.arnugroho.armor.model.common.CommonModel;
import lombok.Data;

@Entity
@Table(name=WarungA.TABLE_NAME)
@Data
public class WarungA //extends CommonModel<Long>
 {
	private static final long serialVersionUID = -7054659686634430552L;
	public static final String TABLE_NAME = "tb_m_warunga";
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
	@TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	private Long id;
	
	@Column(name = "nama")
	private String nama;
	
	@Column(name = "alamat")
	private String alamat;

	
	

}
