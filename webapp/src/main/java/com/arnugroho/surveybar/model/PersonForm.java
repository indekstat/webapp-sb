package com.arnugroho.surveybar.model;

import lombok.Data;

@Data
public class PersonForm {
	private String firstName;
    private String lastName;

}
