package com.arnugroho.surveybar.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name= RSBranch.TABLE_NAME)
@Data
public class RSBranch extends CommonModel{
    public static final String TABLE_NAME = "t_rs_branch";

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "nohp", nullable = false)
    private String nohp;
}
