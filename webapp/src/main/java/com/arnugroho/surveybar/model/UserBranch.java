package com.arnugroho.surveybar.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name= UserBranch.TABLE_NAME)
public class UserBranch extends CommonModel{
    public static final String TABLE_NAME = "t_user_branch";

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "useremail", nullable = false)
    private String userEmail;

    @Column(name = "branchid", nullable = false)
    private String branchId;
}
