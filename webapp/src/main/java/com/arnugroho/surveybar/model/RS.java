package com.arnugroho.surveybar.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name= RS.TABLE_NAME)
public class RS extends CommonModel {
    public static final String TABLE_NAME = "t_rs";

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name", nullable = false)
    private String name;
}
