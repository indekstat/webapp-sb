package com.arnugroho.surveybar.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name= Pasien.TABLE_NAME)
public class Pasien extends CommonModel {
    public static final String TABLE_NAME = "t_pasien";

    @Id
    @Column(name = "norm")
    private String norm;

    @Column(name = "name")
    private String name;

    @Column(name = "shortname")
    private String shortName;

    @Column(name = "tempatlahir")
    private String tempatLahir;

    @Column(name = "tanggallahir")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tanggalLahir;

    @Column(name = "religion")
    private String religion;

    @Column(name = "jeniskelamin")
    private String jenisKelamin;

    @Column(name = "address")
    private String address;

    @Column(name = "notlp")
    private String nomorTelepon;
}
