package com.arnugroho.surveybar.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name=User.TABLE_NAME)
@Data
public class User extends CommonModel{
    public static final String TABLE_NAME = "t_user";

    @Id
    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "nohp", unique = true, nullable = false)
    private String nohp;

}
