package com.arnugroho.surveybar.model.response;

import lombok.Data;

@Data
public class RSBranchResponse {
    private String id;
    private String name;
    private String address;
    private String nohp;
}
