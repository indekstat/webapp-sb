package com.arnugroho.surveybar.model.response;

import lombok.Data;

@Data
public class UserResponse {
    private String name;
    private String username;
    private String email;
}
