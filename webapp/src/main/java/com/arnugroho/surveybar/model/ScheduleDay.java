package com.arnugroho.surveybar.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name= ScheduleDay.TABLE_NAME)
public class ScheduleDay {
    public static final String TABLE_NAME = "t_m_schedule";

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "group_schedule", nullable = false)
    private String groupSchedule;

    @Column(name = "description", nullable = false)
    private String description;

    @OneToMany(targetEntity = ScheduleAppointment.class, mappedBy = "id", orphanRemoval = false, fetch = FetchType.LAZY)
    private Set<ScheduleAppointment> scheduleAppointments;
}
