package com.arnugroho.surveybar.model.response;

import lombok.Data;

@Data
public class DefaultResponse<T> {
    private String message;
    private String status;
    private T data;

    public DefaultResponse(Boolean isSuccess) {
        this.message = "success";
        this.status = "00";
    }
}
