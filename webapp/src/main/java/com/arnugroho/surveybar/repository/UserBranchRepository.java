package com.arnugroho.surveybar.repository;

import com.arnugroho.surveybar.model.UserBranch;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserBranchRepository extends CrudRepository<UserBranch, String> {

    List<UserBranch> findByUserEmail (String email);
}
