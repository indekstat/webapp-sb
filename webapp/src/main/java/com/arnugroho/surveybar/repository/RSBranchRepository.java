package com.arnugroho.surveybar.repository;

import com.arnugroho.surveybar.model.RSBranch;
import com.arnugroho.surveybar.model.response.RSBranchResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RSBranchRepository extends JpaRepository<RSBranch, String> {

    @Query(value = "SELECT * FROM " + RSBranch.TABLE_NAME,nativeQuery = true)
    List<RSBranchResponse> findAllResponse();
}
