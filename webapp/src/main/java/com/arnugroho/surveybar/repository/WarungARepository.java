package com.arnugroho.surveybar.repository;

import org.springframework.data.repository.CrudRepository;

import com.arnugroho.surveybar.model.WarungA;

public interface WarungARepository extends CrudRepository<WarungA, Long>{

}
